import Image from 'next/image'
import dynamic from "next/dynamic";
const Gallary = dynamic(() => import("./components/Gallary"), { ssr: false });
export default function Home() {


  return (
    <div>
      <Gallary/>
    </div>
  )
}


