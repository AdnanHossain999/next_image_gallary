"use client";
import React,{useState,useEffect} from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";




const Gallary = () => {


  const [images, setImages] = useState([
    { src: "/image-0.webp", selected: false },
    { src: "/image-1.webp", selected: false },
    { src: "/image-2.webp", selected: false },
    { src: "/image-3.webp", selected: false },
    { src: "/image-4.webp", selected: false },
    { src: "/image-5.webp", selected: false },
    { src: "/image-6.webp", selected: false },
    { src: "/image-7.webp", selected: false },

  ]);


  useEffect(() => {

    if (typeof window !== "undefined") {
      localStorage.setItem("images", JSON.stringify(images));
      
    }
  }, [images]);




  const onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }

    const reorderedImages = Array.from(images);
    const [movedItem] = reorderedImages.splice(result.source.index, 1);
    reorderedImages.splice(result.destination.index, 0, movedItem);
    setImages(reorderedImages);
  };


  const handleImageClick = (index) => {
    const updatedImages = [...images];
    updatedImages[index].selected = !updatedImages[index].selected;
    setImages(updatedImages);
  };

  const handleDeleteSelected = () => {
    const remainingImages = images.filter((image) => !image.selected);
    setImages(remainingImages);
  };

  return (
    <div className="flex flex-wrap justify-end mx-4 mt-10 p-20">
      <button
        onClick={handleDeleteSelected}
        className="bg-red-500 text-white py-2 px-4 mb-5 "
      >
        Delete Selected
      </button>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="gallery">
          {(provided) => (
            <div
              {...provided.droppableProps}
              ref={provided.innerRef}
              className="flex flex-wrap items-center"
            >
              {images.map((image, index) => (
                <Draggable
                  id={index}
                  draggableId={`image-${index}`}
                  index={index}
                  key={index}
                >
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      className={`w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/4 p-4 ${index === 0 ? "lg:w-2/4 xl:w-2/4" : ""
                        }`}
                    >
                      <div
                        className={`relative ${image.selected
                          ? "border border-blue-500"
                          : ""
                          } rounded-md cursor-pointer hover:bg-red-500 hover:opacity-60 hover:transition-transform duration-300`}
                      >
                        <img
                          src={image.src}
                          alt={`Image ${index + 1}`}
                          className="w-full h-auto object-cover"
                          onClick={() => handleImageClick(index)}
                        />
                        {image.selected && (
                          <div className="absolute top-2 right-2">
                            <FontAwesomeIcon icon={faCheckCircle} color="red" size="2x" />
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>

    </div>

    // <div>
    //   Hello
    // </div>
  );
};
export default Gallary;


